package algorithms;

import java.util.List;

public class BubbleSort <T extends Comparable<? super T>> implements Sorter<T>{

	@SuppressWarnings("unchecked")
	@Override
	public List<T> sort(List<T> list) {
		T[] inputArray = (T[]) new Comparable[list.size()];
		inputArray = list.toArray(inputArray);	// Gör samma sak i insertion sort
		 boolean changed = true; 
		
		   do { 
		        changed = false; 
		        for (int i = 0; i < list.size() - 1; i++) { 
		            if (inputArray[i].compareTo(inputArray[i + 1]) > 0) {
						swap (inputArray, i, i + 1);
		               	changed = true;
		            } 
		        } 
		   } while (changed); 
		   list.clear();
		   for(T element: inputArray) {
			   list.add(element);
		   }
		   return list;
	}
	
	
	private void swap(T[] data, int i, int j) {
			T current = data[i];
			data[i]= data[j];
			data[j] =  current; 
				
	}
	@Override
	public String toString() {
		return getClass().getSimpleName();
		
	}

}
