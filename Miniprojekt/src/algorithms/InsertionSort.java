package algorithms;

import java.util.List;

import listEmptyException.ListEmptyExeption;

public class InsertionSort <T extends Comparable<? super T>> implements Sorter<T> {
    @SuppressWarnings("unchecked")
	@Override
    public List<T> sort(List<T> list){
    	
    	if(list != null) {
    		T[] inputArray = (T[]) new Comparable[list.size()];
    		inputArray = list.toArray(inputArray);
    		for (int i = 1; i < list.size(); i++) {
    			for (int j = i; j >= 1; j--) {
    				if (inputArray[j].compareTo(inputArray[j-1]) > 0) {
    					break;
    				}
					swap(inputArray, j, j-1);
    			}
    		}

    		list.clear();
    		for(T element : inputArray ) {
    			list.add(element);
    		}

    	}else throw new ListEmptyExeption("Empty List");
    		
        return list;
    }
    private void swap(T[] data, int i, int j) {
        T current = data[i];
        data[i] = data[j];
        data[j] = current;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
