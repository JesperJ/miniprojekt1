package algorithms;

import java.util.List;

public class MergeSort <T extends Comparable<? super T>> implements Sorter<T>{

	@SuppressWarnings("unchecked")
	@Override
	public List<T> sort(List<T> list) {
		int size = list.size();
		T[] arrayList = (T[]) new Comparable[size];
		arrayList = list.toArray(arrayList);
		
		T[] tmp = (T[]) new Comparable[size];	
		mergeSortWorker (arrayList, tmp, 0, size - 1); 
		list.clear();
		for(T element: arrayList) {
			list.add(element);	
		}
		return list;
		
	}
	 
	private void mergeSortWorker (T[] arrayList, T[] tmp,  
	     int low, int high) { 
	    int middle, indexLow, indexHigh; 
	     
	    if (high - low >= 1) { 
	        middle = (low + high) / 2; 
	        mergeSortWorker (arrayList, tmp, low, middle); 
	        mergeSortWorker (arrayList, tmp, middle + 1, high); 
	        for (int i= middle; i >= low; i--) 
	            tmp[i] = arrayList[i]; 
	 
	        for (int i= middle + 1; i <= high; i++) 
	            tmp[middle+1+high-i] = arrayList[i]; 
	 
	        indexLow = low; 
	        indexHigh = high; 
	 
	        for (int i = low; i <= high; i++) { 
	            if (tmp[indexHigh].compareTo(tmp[indexLow]) < 0) { 
	                arrayList[i] =  tmp[indexHigh];
	                indexHigh--; 
	            }else { 
	                 arrayList[i] =  tmp[indexLow];
	                 indexLow++; 
	            } 
	        } 
	     }  
	} 
	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
