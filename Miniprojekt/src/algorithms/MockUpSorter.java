package algorithms;

import java.util.List;

public class MockUpSorter<T extends Comparable< ? super T>> implements Sorter<T>
  {
  @Override
  public List<T> sort(List<T> list)
    {
	  return null;
    // here the sorting should take place
    }
  
  @Override
  public String toString()
    {
    return getClass().getSimpleName();
    }
  }
