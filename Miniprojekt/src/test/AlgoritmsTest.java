package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import algorithms.BubbleSort;
import algorithms.InsertionSort;
import algorithms.MergeSort;

class AlgoritmsTest {

static final Integer[] INT_FIXTURE = {1,2,5,4,3};
	
    ArrayList<Integer> emptyList;
    ArrayList<Integer> noneEmptyList;
    BubbleSort<Integer> bubbleSort = new BubbleSort<>();
    InsertionSort<Integer> insertionSort = new InsertionSort<>();
    MergeSort<Integer> mergeSort = new MergeSort<>();
    

    @BeforeEach
    void setUp() {
    	emptyList = new ArrayList<>();
    	noneEmptyList = new ArrayList<>(INT_FIXTURE.length);
    	
    	for(int testData : INT_FIXTURE) {
    		noneEmptyList.add(testData);
    	}
    }
    
    
    @AfterEach
    void tearDown() {
    	noneEmptyList = null;
    	emptyList = null;
    }

    
    /**
	 * Test method for {@link algorithms.MergeSort#sort(java.util.List))}.
	 */
    
    @Test
    void testMergeSort() {
    	assertEquals("[1, 2, 3, 4, 5]", mergeSort.sort(noneEmptyList).toString());
    	assertEquals("[]", mergeSort.sort(emptyList).toString());
     }
    
    /**
	 * Test method for {@link  algorithms.BubbleSort#sort(java.util.List))}.
	 */
    
    @Test
    void testBubbelSort() {
    	assertEquals("[1, 2, 3, 4, 5]", bubbleSort.sort(noneEmptyList).toString());
    	assertEquals("[]", bubbleSort.sort(emptyList).toString());
    }
    
    /**
	 * Test method for {@link algorithms.InsertionSort#sort(java.util.List))}.
	 */
    @Test
    void testInsertionSort() {
    	assertEquals("[1, 2, 3, 4, 5]", insertionSort.sort(noneEmptyList).toString());
    	assertEquals("[]", insertionSort.sort(emptyList).toString());
    }
    
    
}
