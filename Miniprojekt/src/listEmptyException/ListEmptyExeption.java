package listEmptyException;

@SuppressWarnings("serial")
public class ListEmptyExeption extends RuntimeException{

	public ListEmptyExeption(String message) {
		super(message);
	}
}
