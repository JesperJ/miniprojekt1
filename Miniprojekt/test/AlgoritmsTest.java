
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Random;

import algorithms.BubbleSort;
import algorithms.InsertionSort;
import algorithms.MergeSort;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class AlgoritmsTest {

	static final Integer[] INT_FIXTURE = {1,2,5,4,3};
	
    ArrayList<Integer> emptyList;
    ArrayList<Integer> noneEmptyList;
    BubbleSort<Integer> bubbleSort = new BubbleSort<>();
    InsertionSort<Integer> insertionSort = new InsertionSort<>();
    MergeSort<Integer> mergeSort = new MergeSort<>();
    

    @BeforeEach
    void setUp() {
    	emptyList = new ArrayList<>(INT_FIXTURE.length);
    	noneEmptyList = new ArrayList<>(INT_FIXTURE.length);
    	
    	for(int testData : INT_FIXTURE) {
    		noneEmptyList.add(testData);
    	}
    	
    }
    
    
    @AfterEach
    void tearDown() {
    	noneEmptyList = null;
    	emptyList = null;
    }

    @Test
    void testMerge() {
    	assertEquals("1,2,3,4,5", mergeSort.sort(noneEmptyList));
    	
    }
    
}
